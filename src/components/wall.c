#include "walls.h"
#include "../utils/utils.h"

void wallsLevelOne()
{
    hge_transform transform;
    transform.position.x = hgeWindowWidth() * (30.f/100.f);
    transform.position.y = hgeWindowHeight() * (25.f/100.f);
    transform.position.z = 0;
    transform.scale.x = hgeWindowWidth() * (6.f/100.f);
    transform.scale.y = hgeWindowHeight() * (20.f/100.f);
    transform.scale.z = 0;
    createWall(transform);
    transform.position.x = -(hgeWindowWidth() * (30.f/100.f));
    createWall(transform);


    transform.position.y = -(hgeWindowHeight() * (30.f/100.f));
    createWall(transform);
    transform.position.x = hgeWindowWidth() * (30.f/100.f);
    createWall(transform);


    transform.position.y = -((hgeWindowHeight() * (30.f/100.f)) + transform.scale.y/2) + ((hgeWindowHeight() * (7.f/100.f))/2);
    transform.position.x = (hgeWindowWidth() * (30.f/100.f) - transform.scale.x) - (hgeWindowWidth() * (24.f)/100.f);
    transform.scale.y = (hgeWindowHeight() * (7.f/100.f));
    transform.scale.x = (hgeWindowWidth() * (27.f)/100.f)*2;
    createWall(transform);
    transform.scale.y = hgeWindowHeight() * (20.f/100.f);
    transform.position.y = ((hgeWindowHeight() * (25.f/100.f)) + transform.scale.y/2) - ((hgeWindowHeight() * (7.f/100.f))/2);
    transform.scale.y = (hgeWindowHeight() * (7.f/100.f));
    createWall(transform);
}

void createWall(hge_transform transform)
{
    hge_entity* e = hgeCreateEntity();
    hge_transform trans = transform;
    hge_texture color = hgeResourcesQueryTexture("Red");
    tag_component tag;

    hgeAddComponent(e, hgeCreateComponent("Transform", &trans, sizeof(trans)));
    hgeAddComponent(e, hgeCreateComponent("Sprite", &color, sizeof(color)));
    hgeAddComponent(e, hgeCreateComponent("Wall", &tag, sizeof(tag)));
}
