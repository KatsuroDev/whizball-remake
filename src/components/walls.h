#ifndef WB_WALLS_H
#define WB_WALLS_H

#include <HGE/HGE_Core.h>

void wallsLevelOne();
void createWall(hge_transform transform);

#endif
