#ifndef WB_PLAYER_H
#define WB_PLAYER_H

#include "../utils/utils.h"
#include "physic.h"

typedef enum {
    UP, DOWN, NONE
} player_mov_state;

typedef struct {
    int id;
    hge_texture sprite;
    player_mov_state state;
    hge_vec2 movement_direction;
} player_component;

void createPlayer(float x, float y, player_component data, bool playable);
void PlayerMovementSystem(hge_entity* e, hge_transform* transform, rigid_body* body, player_component* player);
void PlayableSystem(hge_entity* e, player_component* player, tag_component* playable);

#endif
