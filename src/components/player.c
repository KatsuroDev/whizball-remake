#include "player.h"

void createPlayer(float x, float y, player_component data, bool playable)
{
    hge_entity* e = hgeCreateEntity();
    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = 0;
    transform.scale.x = hgeWindowWidth() * (2.5f/100.f);
    transform.scale.y = hgeWindowHeight() * (5.f/100.f);
    transform.scale.z = 0;

    rigid_body player_body;
    player_body.vel.x = 0;
    player_body.vel.y = 0;

    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("Sprite", &data.sprite, sizeof(data.sprite)));
    hgeAddComponent(e, hgeCreateComponent("Rigid Body", &player_body, sizeof(player_body)));
    hgeAddComponent(e, hgeCreateComponent("Player", &data, sizeof(data)));
    tag_component playable_tag;
    if(playable) hgeAddComponent(e, hgeCreateComponent("Playable", &playable_tag, sizeof(playable_tag)));
}

void PlayerMovementSystem(hge_entity* e, hge_transform* transform, rigid_body* body, player_component* player)
{
    hge_vec2 velocity = {0, 0};
    float speed = 5.f;
    velocity.x = body->vel.x;
    velocity.y = body->vel.y;
    if(player->state != NONE)
    {
        velocity.x = player->movement_direction.x * speed;
        velocity.y = player->movement_direction.y * speed;
    }
    else
    {
        velocity.x = 0;
        velocity.y = 0;
    }

    body->vel.x = velocity.x;
    body->vel.y = velocity.y;

    transform->position.x += body->vel.x;
    transform->position.y += body->vel.y;
}

void PlayableSystem(hge_entity* e, player_component* player, tag_component* playable)
{
    bool UP_W = hgeInputGetKey(HGE_KEY_W);
    bool DOWN_S = hgeInputGetKey(HGE_KEY_S);

    if(UP_W)
    {
        player->state = UP;
        player->movement_direction.y = 1;
    }
    if(DOWN_S)
    {
        player->state = DOWN;
        player->movement_direction.y = -1;
    }
    if(!DOWN_S && !UP_W)
    {
        player->state = NONE;
        player->movement_direction.y = 0;
        player->movement_direction.x = 0;
    }
}
