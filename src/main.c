#include <HGE/HGE_Core.h>
#include "utils/utils.h"

int main()
{
	hge_window window = {"Whizball Remake", 1920, 1080};
	hgeInit(60, window, HGE_INIT_ALL);

	initGame();

	hgeStart();

	return 0;
}
