#ifndef WB_UTILS_H
#define WB_UTILS_H

#include <HGE/HGE_Core.h>

#define NEAR -100.0f
#define FAR 100.0f


void loadTextures();

void addAllSystems();

void addCamera();

bool isTransformWithinCamFrustrum(hge_transform transform);

void createBackground();

void initGame();

void removeComponent(hge_entity* entity, int index);

void cleanUpGame();

#endif
