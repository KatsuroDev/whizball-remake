#include <HGE/HGE_Core.h>
#include "utils.h"
#include "../components/walls.h"
#include "../components/player.h"

void backgroundRendering(hge_entity* e, tag_component* tag)
{
    hgeClearColor(0.8, 0.8, 0.8, 1);
    hgeClear(0x00004000);
}


void loadTextures()
{
    hgeResourcesLoadTexture("./res/blue.png", "Blue");
    hgeResourcesLoadTexture("./res/green.png", "Green");
    hgeResourcesLoadTexture("./res/red.png", "Red");
}

bool isTransformWithinCamFrustrum(hge_transform transform)
{
    hge_entity* cam = hgeQueryEntity(1, "ActiveCamera");
    hge_camera* camera_data = cam->components[hgeQuery(cam, "Camera")].data;
    hge_vec3* cam_pos = cam->components[hgeQuery(cam, "Position")].data;

    hge_transform camera_transform;
    camera_transform.position = *cam_pos;
    camera_transform.scale.x = hgeWindowWidth() * camera_data->fov;
    camera_transform.scale.y = hgeWindowHeight() * camera_data->fov;

    return AABB(transform, camera_transform);
}

void addAllSystems()
{
    hgeAddSystem(backgroundRendering, 1, "bg");
    hgeAddBaseSystems();
    hgeAddSystem(PlayerMovementSystem, 3, "Transform", "Rigid Body", "Player");
    hgeAddSystem(PlayableSystem, 2, "Player", "Playable");
}

void initGame()
{
    loadTextures();
    addAllSystems();

    addCamera();
    createBackground();

    wallsLevelOne();
    player_component player;
    player.id = 1;
    player.sprite = hgeResourcesQueryTexture("Blue");
    player.movement_direction.x = 0;
    player.movement_direction.y = 0;
    player.state = NONE;
    createPlayer(-hgeWindowWidth() * (25.75f/100.f), 0, player, true);

    player.id = 2;
    player.sprite = hgeResourcesQueryTexture("Green");
    createPlayer(hgeWindowWidth() * (25.75f/100.f), 0, player, false);

}

void addCamera()
{
    hge_entity* camera_entity = hgeCreateEntity();
    hge_camera cam = {true, true, 1.f/1.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth()/(float)hgeWindowHeight(), NEAR-1.0f, FAR+1.0f };
    hge_vec3 camera_position = {0, 0, 0};
    orientation_component camera_orientation = {0.f, -90.0f, 0.f};
    hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
    hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
    hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
    tag_component activecam_tag;
    hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));
}

void createBackground()
{
    hge_entity* e = hgeCreateEntity();
    tag_component tag;
    hgeAddComponent(e, hgeCreateComponent("bg", &tag, sizeof(tag)));
}

void removeComponent(hge_entity* entity, int index) {
  free(entity->components[index].data);
  for(int i = index; i < entity->numComponents; i++) {
    entity->components[i] = entity->components[i+1];
  }
  entity->numComponents--;
}
